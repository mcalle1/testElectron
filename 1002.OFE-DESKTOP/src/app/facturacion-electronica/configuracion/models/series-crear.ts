export class SeriesCrear {
  idEntidad: number;
  serie: string;
  correlativo: string;
  idTipoDocumento: string;
  direccionMac: string;
  tipoSerie: number;
  direccion: string;
}
